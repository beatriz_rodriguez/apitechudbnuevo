package com.techu.apitechudbNuevo.controllers;

import com.techu.apitechudbNuevo.models.ProductModel;
import com.techu.apitechudbNuevo.models.PurchaseModel;
import com.techu.apitechudbNuevo.models.UserModel;
import com.techu.apitechudbNuevo.services.ProductService;
import com.techu.apitechudbNuevo.services.PurchaseService;
import com.techu.apitechudbNuevo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController //Permite devolver un JSON por defecto en el return
@RequestMapping("/apitechu/v2")
@CrossOrigin(origins = "*", methods = {RequestMethod.DELETE,RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT})
public class PurchaseController
{
    @Autowired
    PurchaseService purchaseService;

    @Autowired
    UserService userService;

    @PostMapping("/purchases")
    public ResponseEntity<Object> addPurchase(@RequestBody PurchaseModel purchase){
        System.out.println("addPurchase");
        System.out.println("El id de la compra a añadir es " + purchase.getId());
        System.out.println("El id del usuario que hace la compra es " + purchase.getUserId());
        System.out.println("El importe total de la compra es " + purchase.getAmount());
        System.out.println("Los productos comprados y sus cantidades son " + purchase.getPurchaseItems());

        Optional<UserModel> resultUser = this.userService.getById(purchase.getUserId());

        if (resultUser.isPresent() == true) {
            System.out.println("Usuario encontrado");
            return new ResponseEntity<>(this.purchaseService.add(purchase), this.purchaseService.add(purchase) == "Alguno de los productos no ha sido encontrado" ? HttpStatus.CREATED : HttpStatus.NOT_FOUND) ;

        } else {
            System.out.println("Usuario no encontrado");
            return new ResponseEntity<>("Usuario no encontrado", HttpStatus.NOT_FOUND);
        }


    }

    @GetMapping("/purchases")
    public ResponseEntity<List<PurchaseModel>> getPurchase(){
        System.out.println("getPurchases");

        //return this.productService.findAll();
        return new ResponseEntity<>(this.purchaseService.findAll(), HttpStatus.OK);

    }
}
