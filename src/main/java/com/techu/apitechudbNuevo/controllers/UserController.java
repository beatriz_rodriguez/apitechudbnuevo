package com.techu.apitechudbNuevo.controllers;

import com.techu.apitechudbNuevo.models.ProductModel;
import com.techu.apitechudbNuevo.models.UserModel;
import com.techu.apitechudbNuevo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController //Permite devolver un JSON por defecto en el return
@RequestMapping("/apitechu/v2")
@CrossOrigin(origins = "*", methods = {RequestMethod.DELETE,RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT})
public class UserController {

    @Autowired
    UserService userService;

   // @GetMapping("/users")
   // public ResponseEntity<List<UserModel>>  getUsers() {
   //  System.out.println("getUsers");
   //     return new ResponseEntity<>(this.userService.findAll(), HttpStatus.OK);

   // }

    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel user){
        System.out.println("addUser");

        return new ResponseEntity<>(this.userService.add(user), HttpStatus.CREATED);

    }

    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsersOrderBy(@RequestParam(name="orderBy", required = false) boolean orderBy){
        System.out.println("getUserOrderBy");
        System.out.println("El parametro de ordenacion es "+ orderBy);

        return new ResponseEntity<>(this.userService.findOrdenadoEdad(orderBy), HttpStatus.OK);

    }

    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUserstById (@PathVariable String id){
        System.out.println("getUsertById");
        System.out.println("El id del usuario a buscar es " + id);

        Optional<UserModel> result = this.userService.getById(id);

        if (result.isPresent() == true) {
            return new ResponseEntity<>(result.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Usuario no encontrado", HttpStatus.NOT_FOUND);
        }

    }

    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> updateUser(@RequestBody UserModel user,@PathVariable String id){
        System.out.println("updateUser");
        System.out.println("El id del usuario que se va a actualizar en parámetro es " + id);

        Optional<UserModel> userToUpdate = this.userService.getById(id);

        if (userToUpdate.isPresent()) {
            this.userService.update(user);

        }

        return new ResponseEntity<>(user, userToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable String id){
        System.out.println("deleteUser");
        System.out.println("El id del usuario a borrar es " + id);

        boolean deletedUser = this.userService.delete(id);

        return new ResponseEntity<>( deletedUser ? "Usuario eliminado correctamente" : "Usuario no encontrado",
                deletedUser ? HttpStatus.OK: HttpStatus.NOT_FOUND);

    }


}
