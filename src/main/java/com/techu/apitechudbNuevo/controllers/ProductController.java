package com.techu.apitechudbNuevo.controllers;

import com.techu.apitechudbNuevo.models.ProductModel;
import com.techu.apitechudbNuevo.repositories.ProductRepository;
import com.techu.apitechudbNuevo.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController //Permite devolver un JSON por defecto en el return
@RequestMapping("/apitechu/v2")
@CrossOrigin(origins = "*", methods = {RequestMethod.DELETE,RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT})
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping("/products")
    public ResponseEntity<List<ProductModel>> getProducts(){

        System.out.println("getProducts");

        //return this.productService.findAll();
        return new ResponseEntity<>(this.productService.findAll(), HttpStatus.OK);

        //Abrir conexion a BD
        //Gestionar conexion a BD (comprobar errores al conectar)
        //Preparar consulta a BD
        //Ejecutar consulta a BD
        //Procesar los resultados obtenidos (o devolver error)
        //Mostrar resultados. Paginacion, filtrado posterior
    }

    @PostMapping("/products")
    public ResponseEntity<ProductModel> addProduct(@RequestBody ProductModel product){
        System.out.println("addProduct");
        System.out.println("El id del producto a añadir es " + product.getId());
        System.out.println("La desc del producto a añadir es " + product.getDesc());
        System.out.println("El precio del producto a añadir es " + product.getPrice());
        return new ResponseEntity<>(this.productService.add(product), HttpStatus.CREATED);

    }

    @GetMapping("/products/{id}")
    public ResponseEntity<Object> getProductById (@PathVariable String id){
        System.out.println("getProductById");
        System.out.println("El id del producto a buscar es " + id);

        Optional<ProductModel> result = this.productService.getById(id);

        if (result.isPresent() == true) {
            return new ResponseEntity<>(result.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Producto no encontrado", HttpStatus.NOT_FOUND);
        }

    }

    @PutMapping("/products/{id}")
    public ResponseEntity<ProductModel> updateProduct(@RequestBody ProductModel product,@PathVariable String id){
        System.out.println("updateProduct");
        System.out.println("El id del producto que se va a actualizar en parámetro es " + id);

        Optional<ProductModel> productToUpdate = this.productService.getById(id);

        if (productToUpdate.isPresent()) {
            this.productService.update(product);

        }

        return new ResponseEntity<>(product, productToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/products/{id}")
    public ResponseEntity<String> deleteProduct(@PathVariable String id){
        System.out.println("deleteProduct");
        System.out.println("El id del producto a borrar es " + id);

        boolean deletedProduct = this.productService.delete(id);

        return new ResponseEntity<>( deletedProduct ? "Producto eliminado correctamente" : "Producto no encontrado",
                                    deletedProduct ? HttpStatus.OK: HttpStatus.NOT_FOUND);

    }
}
