package com.techu.apitechudbNuevo.repositories;

import com.techu.apitechudbNuevo.models.UserModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends MongoRepository<UserModel, String> {

}
