package com.techu.apitechudbNuevo.repositories;

import com.techu.apitechudbNuevo.models.PurchaseModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PurchaseRepository extends MongoRepository<PurchaseModel, String> {
}
