package com.techu.apitechudbNuevo.services;

import com.techu.apitechudbNuevo.models.ProductModel;
import com.techu.apitechudbNuevo.models.PurchaseModel;
import com.techu.apitechudbNuevo.models.UserModel;
import com.techu.apitechudbNuevo.repositories.ProductRepository;
import com.techu.apitechudbNuevo.repositories.PurchaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class PurchaseService {

    @Autowired
    PurchaseRepository purchaseRepository;

    @Autowired
    ProductService productService;


    public List<PurchaseModel> findAll(){

        return this.purchaseRepository.findAll();
    }

    public Object add(PurchaseModel purchaseToAdd){
        System.out.println("addPurchase");

        boolean resultfindProduct = true;
        boolean resultfindPurchase = true;
        float totalAmount = 0;
        Optional<ProductModel> productModel;
        float impProducto = 0;

        Set<String> keysProduct =  purchaseToAdd.getPurchaseItems().keySet();

        for ( String key: keysProduct){
            System.out.println("El producto encontrado es " + key);
            productModel = this.productService.getById(key);
            if (!productModel.isPresent()) {
                resultfindProduct = false;
            }
            else {
                System.out.println("El importe del producto " + key + " es " + productModel.get().getPrice());
                System.out.println("La cantidad de productos a comprar es " + purchaseToAdd.getPurchaseItems().get(key));
                totalAmount = (productModel.get().getPrice() * purchaseToAdd.getPurchaseItems().get(key)) + totalAmount;

            }
        }

        if (this.purchaseRepository.existsById(purchaseToAdd.getId())) {
            System.out.println("Ya existe la compra con ese id");
            resultfindPurchase = false;

        }

        if ((resultfindProduct == true) && (resultfindPurchase == true)){

            System.out.println("He encontrado todos los productos");
            System.out.println("El importe total de los productos es " + totalAmount);
            //lo incorporo como el amount
            purchaseToAdd.setAmount(totalAmount);

            return this.purchaseRepository.save(purchaseToAdd);
        }
        else {
            if (resultfindProduct == false) {
                System.out.println("Alguno de los productos no ha sido encontrado");
                return "Alguno de los productos no ha sido encontrado";
            }
            else {
                System.out.println("La compra ha sido encontrado");
                return "La compra ha sido encontrada";

            }
        }




    }

}
