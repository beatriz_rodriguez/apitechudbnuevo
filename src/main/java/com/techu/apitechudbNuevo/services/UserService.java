package com.techu.apitechudbNuevo.services;


import com.techu.apitechudbNuevo.models.ProductModel;
import com.techu.apitechudbNuevo.models.UserModel;
import com.techu.apitechudbNuevo.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public List<UserModel> findAll(){

        return this.userRepository.findAll();
    }

    public List<UserModel> findOrdenadoEdad(boolean indOrder){
        System.out.println("findOrdenado");

        if (indOrder == true) {
            System.out.println("Entro por el ordenado");
            return this.userRepository.findAll(Sort.by(Sort.Direction.ASC, "age"));
        }
        else {
            return this.userRepository.findAll();

        }

    }



    public UserModel add(UserModel user){
        System.out.println("addUser");

        return this.userRepository.save(user);
    }

    public Optional<UserModel> getById(String id){
        System.out.println("getUserById");

        return this.userRepository.findById(id);


    }

    public UserModel update(UserModel user){
        System.out.println("updateUser");

        return this.userRepository.save(user);

    }

    public boolean delete(String id){
        System.out.println("deleteUser");
        boolean result = false;

        if (this.userRepository.findById(id).isPresent()){
            this.userRepository.deleteById(id);
            result = true;
        }

        return result;
    }
}
