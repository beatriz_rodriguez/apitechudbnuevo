package com.techu.apitechudbNuevo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApitechudbNuevoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApitechudbNuevoApplication.class, args);
	}

}
